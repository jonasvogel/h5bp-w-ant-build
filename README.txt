Empty h5bp (https://github.com/h5bp/html5-boilerplate) with ant build script (https://github.com/h5bp/ant-build-script).

How to use:
Open terminal at root of repos and run
$ cd build/
$ ant build
